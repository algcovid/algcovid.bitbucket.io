jQuery(document).ready(function() {
    const apis = [  "https://covid19dz.openalgeria.org/api/v1/stats",
                    "https://covid19dz.openalgeria.org/api/v1/wilayas",
                    "https://covid19dz.openalgeria.org/api/v2/history",
                    "https://covid19dz.openalgeria.org/api/v1/ages/cases",
                    "https://covid19dz.openalgeria.org/api/v1/ages/deaths"];
    const popurl =  "https://bitbucket.org/algcovid/covid19-alg/raw/HEAD/pop.json"// 2008 -_-
    const population = 43851044;
    const globalAPI = "https://corona.lmao.ninja/v2/all?yesterday=false";
    // to store object
    var stats =  [] , wilayat = [] , history= []  , ages=[],agesd=[] , sex=[], globalData = [],popWilayas = [];
    var  spn = "<span class='loader'></span>",
        updating =  '<span class="updating"><span></span></span>' ,
        colors = { },
        gDrate = 0;

    /* charts*/
    var genderchart_, ageschart_;

    // get all abject
    function get(api) {
        return fetch(api)
            .then((resp) => {
                if(!resp.ok) $(".error").slideDown(300)
                return resp.json()
            })
            .then(function (data) {
                return data
            }).catch(e => $(".error").slideDown(300))
    }
    // get all stats [confirmed, deaths, recovred, active]
    get(apis[0]).then(json => {
        stats = json;
        console.log(stats);
        load()
    });
    // get all wilayas
    get(apis[1]).then(json => {
        wilayat = json;
    });

    // get ages (cases & deaths)
    get(apis[3]).then(json => {
        ages = json;
    });
    get(apis[4]).then(json => {
        agesd = json;
    });

    // population
    get(popurl).then(json => {
        popWilayas = json;
    });
    // get worldwide
    get(globalAPI).then(json => {
        globalData = json;
    });


    function global() {
        function commas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        $("#gt").text(commas(globalData.cases));
        $("#gd").text(commas(globalData.deaths));
        $("#ga").text(commas(globalData.active));
        $("#gr").text(commas(globalData.recovered));
        gDrate = ((globalData.deaths / globalData.cases) * 100).toFixed(1)
        $("#gdr").text(" % " + gDrate);
    }
    // load all
    function load () {
        var ts = stats.confirmed.total,
            nt = stats.confirmed.new,
            ds = stats.deaths.total,
            nd = stats.deaths.new,
            rc = stats.recovered.total,
            nr = stats.recovered.new,
            ac = stats.actives,
            lu = stats.dateAsOf,
            dr = ((ds / ts) * 100).toFixed(1),
            percent = (ts / (ts - nt) * 100 - 100).toFixed(2)
        if (percent == 0)  percent = "0"
        else percent += "%"

        toBoxes("على مستوى الوطن", ts, nt + ` (${percent})`, ds, nd, rc, "?", ac, dr, population)
        var tw = 0
        wilayat.map(function(d) {
            colors[d.code]= generateColors(d.confirmed)
            if(d.confirmed > "0") tw++
        });
        $(".tw").text(tw)
        $("#l-u").text(lu + " :آخر تحديث ")
        loadMap();
    }
    // get wilaya data by ID
    function findByID(id, obj) {
        return obj.filter(function(objs) {
                return objs.code == id;
            });
    }
    // insert data into boxes
    function toBoxes (wn,total, tToday, deaths, dToday, recovred, rToday, active,r, popu) {

        var todaySpan;
        $("#title").html(wn);
        todaySpan =  `<span class="difference">&nbsp;+${tToday}</span>`
        $("#total").html(total+todaySpan);
        todaySpan =  `<span class="difference">&nbsp;+${dToday}</span>`
        $("#deaths").html(deaths +todaySpan);
        todaySpan =  `<span class="difference">&nbsp;+${rToday}</span>`
        if(rToday != "?")
            $("#recovred").html(recovred+todaySpan);
        else
            $("#recovred").html(recovred);
        $("#active").html(active);
        if (r == "NaN" || r == 0) r = 0
        else  r+= " %"
        //todaySpan =  `<span class="difference">&nbsp;( &#x1F310; ${gDrate} %)</span>`
        $("#death-rate").html(r);
        $("#cases-m").html(Math.round(total/(popu/1000000)));
        $("#death-m").html(Math.round(deaths/(popu/1000000)));
    }

    function generateColors(red) {
        var redColor = Math.floor((255 * red) / total);
        if(red > 0 && red  <= 10)  return "#FA9B95"
        else if(red > 10 && red <= 50) return "#FB8079"
        else if(red > 50 && red <= 100) return "#EF5350"
        else if(red >  100 && red <=  200) return "#E53935"
        else if(red >  200 ) return "#C62828"
        else return "#cdd2d9"
    }
    function loadMap() {
        $('#vmap').vectorMap({
            map: 'dz_fr',
            backgroundColor: '#222327',
            color: '#262626',
            selectedColor: '#666666',
            enableZoom: true,
            showTooltip: true,
            color: '#279fe0',
            hoverColor: '#b71410',
            selectedColor: '#c79e24',
            colors: colors,
            onRegionSelect: function(event, code, region) {
            },
            onRegionClick: function(event, code, region) {
                $(".box:not(.clicked)").children("h2:first-child").text("");
                 $(".box:not(.state):not(.clicked)").children("h2:first-child").append(spn)
                setTimeout(function() {
                    var wilayaData = findByID(code, wilayat)[0]
                    var popul = findByID(code, popWilayas)[0]
                    console.log(popul);
                    var drate = ((wilayaData.deaths / wilayaData.confirmed) * 100).toFixed(1)
                    toBoxes(wilayaData.name_ar, wilayaData.confirmed, wilayaData.new_cases,
                           wilayaData.deaths, wilayaData.new_cases_death,
                            wilayaData.recovered,"?" ,wilayaData.actives, drate,popul.pop )
                    var sex = [wilayaData.sex.male, wilayaData.sex.female]
                    var agesW = Object.values(wilayaData.ages)

                    ageschart_.updateSeries([{  data: agesW}])
                    genderchart_.updateSeries(sex)
                }, 1000);
                $(".box").toggleClass("pop")
                setTimeout(function() {
                    $(".box").removeClass("pop")
                }, 1000);
                document.querySelector(".statics").scrollIntoView({ behavior: 'smooth' })
            },
            onLabelShow: function(event, label, code) {
                var wilayaData = findByID(code, wilayat)[0]
                var drate = ((wilayaData.deaths / wilayaData.confirmed) * 100).toFixed(1)
                if (drate == "NaN"||drate== 0.0) {
                    drate = 0
                }
                $('.jqvmap-label').html("<span>"+ wilayaData.name_ar + "</span> <br> الإصابات " + wilayaData.confirmed + " <br> الوفيات " + wilayaData.deaths + "<br> المتعافين " + wilayaData.recovered + "<br> معدل الوفيات " + drate + " % ")
            }
        });

        $("#vmap").bind('mousewheel DOMMouseScroll', function(event) {
            if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                 $('#vmap').vectorMap('zoomIn');
            } else {
                 $('#vmap').vectorMap('zoomOut');
            }
        });

        genderChart([stats.gender.male, stats.gender.female])
        agesChart(Object.values(ages),Object.keys(ages))
        global()
    }
    function sortByProperty(property){
       return function(a,b){
          if(a[property] > b[property])
             return 1;
          else if(a[property] < b[property])
             return -1;
          return 0;
       }
    }
    $(".time").click(function() {
        $(".g-wrapper").fadeOut(100)
        $(this).toggleClass("toggle")
         $(".chart-wrapper").slideUp(200, function() {
            $(".time-wrapper").slideToggle(500);
            setTimeout(function() {
                 displayStatics ()
                if( $(".time-wrapper").children("iframe").length != 1) {
                    $(".time-wrapper").append('<iframe id="time-frame" frameborder ="0" src="timeline.html" allowfullscreen></iframe>')
                    document.getElementById('time-frame').onload = function() {
                        $(".time-wrapper > .loader").remove()
                        $('#time-frame').slideDown(500)
                        $(this).css("z-index", "5")
                    }
                }
            }, 100);
        })
    })
    $(".chart").click(function() {
        $(".g-wrapper").fadeOut(100)
        $(this).toggleClass("toggle")
         $(".time-wrapper").slideUp(200, function() {
            $(".chart-wrapper").slideToggle(500);
            setTimeout(function() {
                 displayStatics ()
                if( $(".chart-wrapper").children("iframe").length != 1) {
/*                    $(".chart-wrapper").html('<object type="text/html" data="charts.html" ></object>')*/
                    $(".chart-wrapper").append('<iframe id="chart-frame" frameborder ="0" src="charts.html" allowfullscreen></iframe>')
                    document.getElementById('chart-frame').onload = function() {
                        $(".chart-wrapper > .loader").remove()
                        $('#chart-frame').slideDown(500)
                    }
                }
            }, 100);
        });
    })
    $('.down').click(function () {
            $('html, body').animate({
                scrollTop: $("#vmap").offset().top
            }, 500);
    });
    $(".reload").click(function() {
       location.reload(true);
    });
    $(".global").click(function() {
        $(".g-wrapper").fadeToggle(100)
    })
    $(".clicked").click(function() {
        $(".ages").slideToggle(200)
    })
    $(".error").click(function() {
        $(this).fadeOut(200);
    })
    // close worldwide whene map clicked
    $("#vmap").mouseup(function (e){
        if (!$(".global").is(e.target) && $(".global").has(e.target).length === 0){
            $(".g-wrapper").fadeOut();
        }
    });
    function displayStatics() {
        $("section").toggleClass("section-ontimer");
        if($("section").hasClass("section-ontimer")) {
            $(".statics").css("display", "none")
        } else {
            $(".statics").css("display", "grid")
        }
    }

    /* smal charts */
    function genderChart(data) {

        var options = {
            series: data ,

            chart: {
                width: '100%',
                type: 'pie',
            },

            colors: ['#fc5a49', '#4a2f74' ],
            dataLabels: {
                enabled: true
            },
            labels: ["ذكور", "إناث"],
            stroke: {
                width: 1,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        offset:  -20,
                    }
                }
            },
            dataLabels: {
                formatter(val, opts) {
                    const name = opts.w.globals.labels[opts.seriesIndex]
                    return [name, val.toFixed(1) + '%']
                },
                style: {
                    fontSize: '12px',
                    fontFamily: 'HelveticaNeue'
              },
            },
            legend: {
                show: false
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: "HelveticaNeue",
                },
            },
        };
        genderchart_ = new ApexCharts(document.querySelector("#gender-chart"), options);
        genderchart_.render();
    }
    function agesChart(data, labels) {
        window.Apex = {
            chart: {
                foreColor: '#ccc',
                fontFamily: "HelveticaNeue"
            },
            tooltip: {
                shared: true,
                theme: 'dark'
            },
            grid: {
                borderColor: "rgba(73, 78, 96, 0.29)",
                xaxis: {
                    lines: {
                        show: true
                    }
                }
            }
        };
        var options = {
            series: [{
                data: data,
                name: 'المصابين',
            }],
            chart: {
                height: "100%",
                width: "100%",
                type: 'bar',
            },
            colors: ['#fc5a49'],
            plotOptions: {
                bar: {
                    columnWidth: '80%',
                    distributed: true
                }
            },
            dataLabels: {
                enabled: true,
                position: 'center',
                style: {
                    fontSize: '9px',
                    color:"#141414"
                }
            },
            legend: {
                show: false
            },
            xaxis: {
                categories: labels,
                labels: {
                    style: {

                        fontSize: '10px'
                    }
                }
            },
            title: {
                text: 'أعمار المصابين',
                align: 'center',
                style: {
                    fontSize: "13px"
                }
            },
        };
        ageschart_ = new ApexCharts(document.querySelector("#ages-chart"), options);
        ageschart_.render()
    }
});
