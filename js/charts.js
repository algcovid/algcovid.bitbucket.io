function _(id) {
    return document.getElementById(id);
};
//########## deprecated -- using open algeria api instead
const url = "https://bitbucket.org/algcovid/covid19-alg/raw/HEAD/all-in-one.json";
const daily = "https://bitbucket.org/algcovid/covid19-alg/raw/HEAD/dailyCases.json";
//##############################################################################
const historyAPI = "https://covid19dz.openalgeria.org/api/v2/history";
var data = [], ccat = [],arr2 = [],arr3 = [];
var chart, chart2, chart3, chart4;
var confirmed, deaths, recovered, categories = [], dailyConfirmed = [], dailyDeaths = [],
    dailyRecovered = [],predict = [], deathsPredict = [];
// get all abject
function get(api) {
    return fetch(api)
        .then((resp) => resp.json())
        .then(function (data) {
            return data
        })
}
// history
get(historyAPI ).then(json => {
    // get data just from the first case 25-02-2019
    data = json.slice(34, json.length - 1)
    confirmed = Array.from(data, ({confirmed}) => confirmed)
    recovered = Array.from(data, ({recovered}) => recovered)
    deaths = Array.from(data, ({deaths}) => deaths)
    categories = Array.from(data, ({date}) => date.substring(5))
    // get differance between today and last day in the whole timeline
    confirmed.map(function(a, b) {
        b > 0 ? dailyConfirmed.push(a - confirmed[b-1]):dailyConfirmed.push(0)
    });
    deaths.map(function(a, b) {
        b > 0 ? dailyDeaths.push(a - deaths[b-1]):dailyDeaths.push(0)
    });
    recovered.map(function(a, b) {
        b > 0 ? dailyRecovered.push(a - recovered[b-1]):dailyRecovered.push(0)
    });
    categories = categories.concat("+1", "+2", "+3", "+4", "+5" );
    sevendaysDevider(dailyConfirmed, arr2);
    sevendaysDevider(dailyDeaths, arr3);
    forcats4Days(confirmed,predict,confirmed.length - 30);
    confirmed = confirmed.concat(predict);
    forcats4Days(deaths,deathsPredict,deaths.length - 30);
    deaths = deaths.concat(deathsPredict);
    startChart1(confirmed, deaths,categories)
    removeLoaders();
});
function removeLoaders () {
  document.querySelectorAll(".loader").forEach(el => el.remove());
 }
function timecvrt(timestamp) {
    var a = new Date(timestamp);
    var months = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = " آخر تحديث: " + date + ' ' + month + ' ' + year + ' [' + hour + ':' + min + ':' + sec + ']';
    dataTime = month + ' ' + date;
    return time;
}
function spread(c) {
    let N = c.length;
    let rates = new Array(N);
    for (let i = 0; i <= N - 2; i++) {
        rates.push((c[i + 1] - c[i]) / c[i])
    }
    return rates;
}
function pri(c, d, r, p) {
    for (let i = 0; i < d; i++) {
        c = c * (1 + r);
        p.push(Math.floor(c))
    }
    return c;
}
function forcats4Days(c, p, s) {
    var d = c.slice(s, c.length);
    var rates = spread(d);
    var average = rates.reduce((previous, current) => current += previous) / c.length;
    console.log(Math.floor(pri(c[c.length - 1], 4, average,p)));
}
function sevendaysDevider(ser, ar2) {
    var arr = ser;
    ccat = [];
    var c = 0
    var k = 0;
    for (let i = 0; i < arr.length; i++) {
        if (i % 7 == 0 && i != 0) {
            k++;
            ccat.push(k)
            c += arr[i]
            ar2.push(c)
            c = 0
        } else c += arr[i]
    }
    if (k * 7 != arr.length) {
        c = 0;
        for (let i = k * 7 + 1; i < arr.length; i++) {
            c += arr[i]
        }
        k++;
        ccat.push(k)
        ar2.push(c)
    }
}
// main
function startChart1(datai, dths, cat) {
    setTimeout(function () {
        chart = new ApexCharts(document.querySelector("#chart"),
            options = {
                series: [
                    { data: datai, name: "إجمالي المصابين" },
                    { data: dths, name: "إجمالي المتوفين" }
                ],
                chart: {
                    height: "96%",
                    type: 'area',
                    stacked: false,
                    foreColor: "#999",
                    zoom: {
                        enabled: true,
                        type: 'x',
                        autoScaleYaxis: true,
                        zoomedArea: {
                            fill: {
                                color: '#90CAF9',
                                opacity: 0.3
                            },
                            stroke: {
                                color: '#0D47A1',
                                opacity: 0.3,
                                width: 1
                            }
                        }
                    }
                },
            //------------------------------

            //--------------------------------
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: "smooth",
                    width: 4
                },
                grid: {
                    padding: {
                        right: 10
                    }
                },
                title: {
                    text: 'إجمالي الإصابات والمتوفين (لوغارتمي)',
                    align: 'center'
                },
                legend: {
                    show: true,
                },
                //---------------------------- X
                xaxis: {
                    type: 'category',
                    categories: cat,
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: true
                    },
                    crosshairs: {
                        show: true,
                        position: 'back',
                        stroke: {
                            color: 'rgba(250,250,250,.2)',
                            width: 1,
                            dashArray: 2,
                        },
                    },
                    labels: {
                        show: true,
                        rotate: -45,
                        rotateAlways: true,
                        trim: true,
                        style: {
                            colors: "#FFF",
                            fontSize: '10px',
                        },
                    },
                    tooltip: {
                        enabled: false,
                    },
                },
                //-------------------- Y
                yaxis: [
                    {
                    labels: {
                        show: true,
                        style: {
                            colors: "#FFF",
                            fontSize: '9px',
                        },
                    },
                    tickAmount: 20,
                    min: 1,
                    max: 10000,
                    forceNiceScale: false,
                    logarithmic: true,
                    crosshairs: {
                        show: true,
                        position: 'back',
                        stroke: {
                            color: 'rgba(250,250,250,.2)',
                            width: 1,
                            dashArray: 2,
                        },
                    },
                    title: { text: "المصابين" },
                  },
                    {
                    labels: {
                        show: true,
                        style: {
                            colors: "#FFF",
                            fontSize: '9px',
                        },
                    },
                    tickAmount: 20,
                        min: 1,
                    max: 1000,
                    forceNiceScale: false,
                    logarithmic: false,
                    opposite: true,
                    title: { text: "المتوفين" }
                  }
                ],
                //----------------------- ANNOTATIONS
                annotations: {
                    yaxis: [
                        /*{
                            y: 6000,
                            borderWidth: 4,
                            strokeDashArray: 0,
                            borderColor: '#F00',
                            fillColor: '#F00',
                            label: {
                                offsetX: -300,
                                borderColor: '#F00',
                                style: {
                                    fontSize: '13px',
                                    color: '#fff',
                                    background: '#F00',
                                },
                                text: ' خطر انهيار المنظومة الصحية إن تجاوزنا حاجز 6000 حالة سريرية',
                            }
                    },*/
                    ],
                    xaxis: [
                        {
                            x: categories[23],
                            x2: categories[7],
                            strokeDashArray: 1,
                            borderColor: '#000',
                            fillColor: '#775DD0',
                            label: {
                                offsetX: 30,
                                offsetY: 150,
                                orientation: 'horizontal',
                                borderColor: '#775DD0',
                                style: {
                                    fontSize: '12px',
                                    color: '#fff',
                                    background: '#775DD0',
                                },
                                text: 'منطقة الإستهتار 🡒',
                            }
                        }, {
                            x: categories[23],
                            strokeDashArray: 4,
                            borderColor: '#000',
                            borderWidth: 3,
                            fillColor: '#000',
                            label: {
                                offsetY: 230,
                                orientation: 'horizontal',
                                borderColor: '#000',
                                style: {
                                    fontSize: '12px',
                                    color: '#fff',
                                    background: '#000',
                                },
                                text: " !! " + 'توقيف الرحلات الجوية من والى أوربا',
                            }
                        },
                        {
                            x: categories[7],
                            x2: categories[5],
                            strokeDashArray: 0,
                            borderWidth: 0,
                            borderColor: 'rgba(0, 255, 69, 0.22)',
                            fillColor: 'rgba(0, 255, 69, 0.22)',
                    }, {
                            x: categories[5],
                            borderColor: '#00ff45',
                            borderWidth: 3,
                            strokeDashArray: 0,
                            borderColor: '#00ff45',
                            label: {
                                orientation: 'horizontal',
                                borderColor: '#00ff45',
                                style: {
                                    fontSize: '12px',
                                    color: '#141414',
                                    background: '#00ff45',
                                },
                                text: '🡐 منطقة الأمان',
                                offsetY: 0,
                                offsetX: 0,
                            }
                        },
                        {
                            x: categories[categories.length - 2],
                            x2: categories[categories.length - 5],
                            strokeDashArray: 0,
                            borderWidth: 2,
                            borderColor: '#ff8900',
                            fillColor: "#ff8900",
                            opacity: 0.4,
                            label: {
                                offsetY: 245,
                                offsetX: -120,
                                orientation: 'horizontal',
                                borderColor: '#ff8900',
                                style: {
                                    fontSize: '12px',
                                    color: '#000',
                                    background: '#ff8900',
                                },
                                text: 'مجال التوقعات +4 إعتمادا على آخر 30 يوم  ⇗',
                            }
                        },
                        {
                            x: categories[categories.length - 5],
                            strokeDashArray: 0,
                            borderWidth: 2,
                            borderColor: '#ff8900',
                        },
                    ],
                    points: [
                        {
                            x: categories[categories.length - 6],
                            y: confirmed[confirmed.length - 5],
                            marker: {
                                size: 4,
                                fillColor: '#120506',
                                strokeColor: '#bb2124',
                                radius: 2,
                            },
                            label: {
                                borderColor: '#fff',
                                offsetY: 45,
                                offsetX: -30,
                                style: {
                                    color: '#fff',
                                    fontSize: '12px',
                                    background: '#000',
                                },
                                text: 'الحــالية↗',
                            }
                        }, {
                            x: categories[35],
                            y: 716,
                            marker: {
                                size: 8,
                                fillColor: '#b2611a',
                                strokeColor: '#b2611a',
                                radius: 2,
                            },
                            label: {
                                borderColor: '#b2611a',
                                offsetY: 0,
                                offsetX: 0,
                                style: {
                                    color: '#fff',
                                    fontSize: '14px',
                                    background: '#b2611a',
                                },
                                text: " ! " + 'رفع عدد الفحوصات اليومي الى 250 ',
                            }
                        }, {
                            x: categories[23],
                            y: 90,
                            marker: {
                                size: 8,
                                fillColor: '#000',
                                strokeColor: '#000',
                                radius: 2,
                            }
                        }
                    ],
                },
                //----------------------- ANNOTATIONS
            }
        );
        chart.render();
        startChart2()
    }, 500);
}
// weeks
function startChart2() {
    setTimeout(function () {
        chart2 = new ApexCharts(document.querySelector("#chart2"),
            options = {
                series: [
                    {
                        name: 'المصابين',
                        data: arr2
                    },
                    {
                        name: 'الوفيات',
                        data: arr3
                    }
                ],
                chart: {
                    type: 'bar',
                    height: "96%",
                    stacked: true,
                },
                colors: ['#F09238', '#e5004c'],
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '100%',
                    },
                },
                dataLabels: {
                    enabled: true,
                    position: 'center',
                    style: {
                        fontSize: '9px',
                    }
                },
                fill: {
                    type: 'pattern',
                    opacity: 1,
                    pattern: {
                        style: [, 'slantedLines'], // string or array of strings
                    }
                },
                stroke: {
                    width: 1,
                    colors: "#fff"
                },
                grid: {
                    xaxis: {
                        lines: {
                            show: true
                        }
                    }
                },
                legend: {
                    position: 'top',
                    horizontalAlign: 'right',
                    offsetX: 40
                },
                yaxis: {
                },
                tooltip: {
                    shared: true,
                    x: {
                        formatter: function (val) {
                            return val + " الأسبوع "
                        }
                    },
                    y: {
                        formatter: function (val) {
                            return Math.abs(val)
                        }
                    }
                },
                title: {
                    text: 'إجمالي المصابين والوفيات الأسبوعي',
                    align: 'center',
                    style: {
                        fontSize: "13px"
                    }
                },
                xaxis: {
                    categories: ccat,
                    title: {
                        text: 'الأسابيع',
                    },
                    labels: {
                        formatter: function (val) {
                            //return Math.abs(Math.round(val)) +
                            return Math.abs(val)
                        }
                    }
                },
            }
        );
        chart2.render();
        startChart3()
    }, 500);
}
//daily cases
function startChart3() {
    chart3 = new ApexCharts(document.querySelector("#chart4"),
        options = {
            series: [
                {
                    data: dailyConfirmed,
                    name: "المصابين"
                },
                {
                    data: dailyRecovered,
                    name: "المتعافين"
                },
                {
                    data: dailyDeaths,
                    name: "الوفيات"
                },
                ],
            chart: {
                height: "96%",
                type: 'bar',
                stacked: true,
            },
            colors: ['#ff8900','#2dea60' , '#F00'],
            dataLabels: {
                enabled: false
            },
            stroke: {
               curve: 'smooth',
                width: 2
            },
            title: {
                text: 'الحالات اليومية',
                align: 'center'
            },
            legend: {
                show: false
            },
            xaxis: {
                type: 'categories',
                categories: [...Array(dailyConfirmed.length).keys()].map(x => ++x),
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: true
                },
                tooltip: {
                    enabled: false,
                    style: {
                        fontSize: '12px',
                    },
                },
                crosshairs: {
                    show: true,
                    position: 'back',
                    stroke: {
                        color: 'rgba(250,250,250,.2)',
                        width: 1,
                        dashArray: 2,
                    },
                },
                labels: {
                    show: true,
                    rotate: -45,
                    rotateAlways: true,
                    trim: true,
                    style: {
                        colors: "#FFF",
                        fontSize: '9px',
                    },
                },
            },
            yaxis: [
                {
                    labels: {
                        show: true,
                        style: {
                            colors: "#FFF",
                            fontSize: '12px',
                        },
                    },
                    tooltip: {
                        enabled: true,
                        style: {
                            fontSize: '12px',
                        },
                    },
                    tickAmount: 3,
                    min: 1,
                    logarithmic: false,
                    crosshairs: {
                        show: true,
                        position: 'back',
                        stroke: {
                            color: 'rgba(250,250,250,.2)',
                            width: 1,
                            dashArray: 2,
                        },
                    },
                    }
                ],
            tooltip: {
                enabled: true,
                shared: true,
                theme: 'dark',
                x: {
                        formatter: function (val) {
                            return val + " اليوم "
                        }
                    },
                style: {
                    fontSize: '12px',
                }
            },
        }
    );
    chart3.render();
}
window.Apex = {
    chart: {
        foreColor: '#ccc',
        fontFamily: "HelveticaNeue"
    },
    colors: ['#ff8900', '#F00','#2dea60' ],
    stroke: {
        curve: "smooth",
        width: 3
    },
    tooltip: {
        shared: true,
        theme: 'dark'
    },
    grid: {
        borderColor: "rgba(73, 78, 96, 0.29)",
        xaxis: {
            lines: {
                show: true
            }
        }
    }
};
function scrollToChart(ele) {
    document.querySelector(ele).scrollIntoView({ behavior: 'smooth' })
}
_("tod").addEventListener('click', function(e) {
    scrollToChart("#chart4")
});
_("tow").addEventListener('click', function(e) {
    scrollToChart("#chart2")
});
_("toc").addEventListener('click', function(e) {
    scrollToChart("#chart")
});

/*
function toggleFullscreen(elem) {
    elem = elem || document.documentElement;
    if (!document.fullscreenElement && !document.mozFullScreenElement &&
        !document.webkitFullscreenElement && !document.msFullscreenElement) {
        if (elem.requestFullscreen) {
            elem.parentElement.parentElement.classList.add("full")
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.parentElement.parentElement.classList.add("full")
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.parentElement.parentElement.classList.add("full")
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.parentElement.parentElement.classList.add("full")
            elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
            elem.parentElement.parentElement.classList.remove("full")
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
            elem.parentElement.parentElement.classList.remove("full")
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
            elem.parentElement.parentElement.classList.remove("full")
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
            elem.parentElement.parentElement.classList.remove("full")
        }
    }
}
_('chart').addEventListener('click', function() {
    toggleFullscreen(this);
});
_('chart2').addEventListener('click', function() {
    toggleFullscreen(this);
});
_('chart3').addEventListener('click', function() {
    toggleFullscreen(this);
});
_('chart4').addEventListener('click', function() {
    toggleFullscreen(this);
});
*/
