/*global $ */
$(function () {
    var dates = [],
        headlines = [],
        texts = [],
        media = [];
    // don't like ajax ^_^
    function getData() {
        var timeLineRequest = new XMLHttpRequest();
        timeLineRequest.onreadystatechange = function () {
            if (timeLineRequest.readyState == 4) {
                var all = JSON.parse(timeLineRequest.responseText);
                all.timeline.map(function (d) {
                    dates.push(d.startDate);
                    headlines.push(d.headline);
                    texts.push(d.text);
                    media.push(d.media);
                })
                if (timeLineRequest.status === 200 || timeLineRequest.status == 0) {
                    makeeRect();
                }
            }
        }
        timeLineRequest.open("GET", "https://bitbucket.org/algcovid/covid19-alg/raw/HEAD/timeline.json", true);
        timeLineRequest.send();
    }
    getData()
    var monthSpan = ["جــانفي", "فــيفري", "مـــارس", "أفريل", "مـــاي", "جــوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
    function dateSpan(date) {
        var month = date.split('/')[0];
        //month = monthSpan[month - 1];
        var day = date.split('/')[1];
        if (day.charAt(0) == '0') {
            day = day.charAt(1);
        }
        var year = date.split('/')[2];
        return month + "-" + day ;
    }
    function makeeRect() {
        //if there's only one date. Who needs it!?
        if (dates.length < 2) {
            $("#line").hide();
            $("#span").show().text(dateSpan(dates[0]));
        } else if (dates.length >= 2) {
            //Set day, month and year variables for the math
            var first = dates[0];
            var last = dates[dates.length - 1];
            var firstMonth = parseInt(first.split('/')[0]);
            var firstDay = parseInt(first.split('/')[1]);
            var lastMonth = parseInt(last.split('/')[0]);
            var lastDay = parseInt(last.split('/')[1]);
            //Integer representation of the last day. The first day is represnted as 0
            var lastInt = ((lastMonth - firstMonth) * 30) + (lastDay - firstDay);
            //Draw first date circle
            $("#line").append('<div class="circle" id="circle0" style="left: ' + 0 + '%;"><div class="popupSpan">' + dateSpan(dates[0]) + '</div></div>');
            $("#mainCont").append('<span id="span0" class="center">' + dateSpan(dates[0]) + '<div class="content"> <div class="media"><iframe frameborder ="0" src=' + media[0] + 'allow="encrypted-media; gyroscope; picture-in-picture"></iframe></div><div class="text"><h2>' + headlines[0] + '</h2><p>' + texts[0] + ' </p></div></div></span>');
            //Loop through middle dates
            for (var i = 1; i < dates.length - 1; i++) {
                var thisMonth = parseInt(dates[i].split('/')[0]);
                var thisDay = parseInt(dates[i].split('/')[1]);
                //Integer representation of the date
                var thisInt = ((thisMonth - firstMonth) * 30) + (thisDay - firstDay);
                //Integer relative to the first and last dates
                var relativeInt = thisInt / lastInt;
                //Draw the date circle
                $("#line").append('<div class="circle" id="circle' + i + '" style="left: ' + relativeInt * 100 + '%;"><div class="popupSpan">' + dateSpan(dates[i]) + '</div></div>');
                $("#mainCont").append('<span id="span' + i + '" class="right">' + dateSpan(dates[i]) + '<div class="content"> <div class="media"><iframe frameborder ="0" src=' + media[i] + ' allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div><div class="text"><h2>' + headlines[i] + '</h2><p>' + texts[i] + ' </p></div></div></span>');
            }
            //Draw the last date circle
            $("#line").append('<div class="circle" id="circle' + i + '" style="left: ' + 99 + '%;"><div class="popupSpan">' + dateSpan(dates[dates.length - 1]) + '</div></div>');
            //$("#mainCont").append('<span id="span' + i + '" class="right">' + dateSpan(dates[i]) + '</span>');
            $("#mainCont").append('<span id="span' + i + '" class="right">' + dateSpan(dates[i]) + '<div class="content"> <div class="media"><iframe frameborder ="0" src=' + media[i] + '  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div><div class="text"><h2>' + headlines[i] + '</h2><p>' + texts[i] + ' </p></div></div></span>');
        }
        $(".circle:first").addClass("active");
        $(".circle").mouseenter(function () {
            $(this).addClass("hover");
        });
        $(".circle").mouseleave(function () {
            $(this).removeClass("hover");
        });
        $(".circle").click(function () {
            var spanNum = $(this).attr("id");
            selectDate(spanNum);
        });
    }
    function selectDate(selector) {
        $selector = "#" + selector;
        $spanSelector = $selector.replace("circle", "span");
        var current = $selector.replace("circle", "");
        $(".active").removeClass("active");
        $($selector).addClass("active");
        if ($($spanSelector).hasClass("right")) {
            $(".center").removeClass("center").addClass("left")
            $($spanSelector).addClass("center");
            $($spanSelector).removeClass("right")
        } else if ($($spanSelector).hasClass("left")) {
            $(".center").removeClass("center").addClass("right");
            $($spanSelector).addClass("center");
            $($spanSelector).removeClass("left");
        };
    };
});
